<?php

namespace Drupal\agoraparagraphs\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Backported agoraparagraphs Drush commands.
 */
class AgoraparagraphsServiceCommands extends DrushCommands {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The paragraphs storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $paragraphsStorage;

  /**
   * Constructs a new AgoraparagraphsServiceCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->paragraphsStorage = $entity_type_manager->getStorage('paragraph');
  }

  /**
   * Cleanup (delete) orphaned paragraphs.
   *
   * Currently only children of nodes are considered. And the query is based on
   * the recorded usage table of entity_usage module. So this module must be
   * installed and the paragraph entities must be counted, otherwise great data
   * loss is possible. Also, revisions are not considered at all. So do not use
   * this on sites with revision support.
   *
   * @command agoraparagraphs:cleanup-paragraphs
   *
   * @usage agoraparagraphs:cleanup-paragraphs
   *   Cleanup (delete) orphaned paragraphs.
   *
   * @aliases agp:cup
   */
  public function cleanupOrphanedParagraphs() {
    $query = $this->database->select('paragraphs_item_field_data', 'pifd');
    $query->fields('pifd', ['id']);
    $subquery = $this->database->select('entity_usage', 'eu');
    $subquery->fields('eu', ['target_id']);
    $library_query = $this->database->select('paragraphs_library_item_field_data', 'plifd');
    $library_query->fields('plifd', ['paragraphs__target_id']);
    $subquery->condition('target_type', 'paragraph');
    $query->condition('pifd.parent_type', ['node', 'taxonomy_term', 'paragraph'], 'IN');
    $query->condition('pifd.id', $subquery, 'NOT IN');
    $query->condition('pifd.id', $library_query, 'NOT IN');
    $orphan_ids = $query->execute()->fetchCol();

    if (empty($orphan_ids)) {
      $this->writeln('No orphaned paragraph entities found.');
      return;
    }

    /** @var \Drupal\paragraphs\Entity\Paragraph[] $orphans */
    $orphans = $this->paragraphsStorage->loadMultiple($orphan_ids);
    $this->paragraphsStorage->delete($orphans);
    $this->writeln(sprintf('%s orphaned paragraph entities deleted.', count($orphan_ids)));
  }

}
