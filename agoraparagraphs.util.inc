<?php

/**
 * @file
 * Utility functions of the agoraparagraphs module.
 *
 * Include this file with the following command:
 * \Drupal::moduleHandler()->loadInclude('agoraparagraphs', 'inc', 'agoraparagraphs.util');
 */

use Drupal\Core\Field\FieldConfigBase;
use Drupal\field\Entity\FieldConfig;

/**
 * Define, which paragraph bundles are allowed for the field_paragraphs field.
 *
 * @param string $node_type
 *   The node type for which we define the allowed bundles.
 * @param string[] $paragraph_bundles
 *   The list of paragraph bundles to allow.
 * @param bool $merge
 *   Whether to merge with existing setting (== add to existing list) or
 *   completely override the list. Defaults to FALSE.
 */
function agoraparagraphs_set_allowed_paragraph_bundles(string $node_type, array $paragraph_bundles = [], bool $merge = FALSE) {
  if (!$fields = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties([
    'entity_type' => 'node',
    'bundle' => $node_type,
    'field_name' => 'field_paragraphs',
  ])) {
    return;
  }
  if (empty($paragraph_bundles)) {
    // Do not allow resetting by now.
    return;
  }
  $paragraph_bundles = array_combine($paragraph_bundles, $paragraph_bundles);

  /** @var \Drupal\Core\Field\FieldConfigInterface $field */
  $field = reset($fields);
  $handler_settings = $field->getSetting('handler_settings');
  $handler_settings['negate'] = 0;
  $should_override = !$merge || empty($handler_settings['target_bundles']);

  if ($should_override) {
    $handler_settings['target_bundles'] = $paragraph_bundles;
    $weight = 0;
    foreach ($paragraph_bundles as $bundle) {
      $handler_settings['target_bundles_drag_drop'][$bundle] = [
        'enabled' => TRUE,
        'weight' => $weight,
      ];
      $weight++;
    }
  }
  else {
    $handler_settings['target_bundles'] = array_merge($handler_settings['target_bundles'], $paragraph_bundles);

    $max_weight = -1;
    if (!empty($handler_settings['target_bundles_drag_drop'])) {
      foreach ($handler_settings['target_bundles_drag_drop'] as &$existing_bundle_setting) {
        $max_weight = max($max_weight, $existing_bundle_setting['weight']);
        // Disable here, will be enabled below, if needed.
        $existing_bundle_setting['enabled'] = FALSE;
      }
    }

    foreach ($handler_settings['target_bundles'] as $bundle) {
      $handler_settings['target_bundles_drag_drop'][$bundle] = [
        'enabled' => TRUE,
        'weight' => $handler_settings['target_bundles_drag_drop'][$bundle]['weight'] ?? ++$max_weight,
      ];
    }
  }
  $field->setSetting('handler_settings', $handler_settings)->save();
}

/**
 * Sets the allowed classy paragraphs for the given paragraph bundle.
 *
 * This is a utility function, tailored for our needs during custom module
 * installations. It is hardcoded assumed, that the classy paragraph reference
 * field's name is 'field_css'. No matter what field setting was used before,
 * after calling this function, the 'classy_paragraphs' handler will be used.
 *
 * @param string $bundle
 *   The paragraph bundle name to modify. This bundle must have a 'field_css'
 *   reference field to a classy paragraph.
 * @param array $target_bundles
 *   The target bundles to set. Dependent on the $negate parameter, these are
 *   either classy paragraphs that should be included or excluded.
 * @param bool $merge
 *   Whether to merge the given bundles to existing setting. Defaults to TRUE.
 *   Please note that merging can only be done, if the 'classy_paragraphs'
 *   handler is already used, and the filter type is set to 'target_bundles'.
 * @param bool $negate
 *   Whether to negate the given target bundles. Defaults to TRUE.
 */
function agoraparagraphs_set_allowed_classy_paragraphs(string $bundle, array $target_bundles, bool $merge = TRUE, bool $negate = TRUE) {
  $field = FieldConfig::loadByName('paragraph', $bundle, 'field_css');
  if (empty($field)) {
    return;
  }
  assert($field instanceof FieldConfigBase);
  if (!empty($target_bundles)) {
    $target_bundles = array_combine($target_bundles, $target_bundles);
  }
  $field_settings = $field->getSettings();

  if ($field_settings['handler'] !== 'classy_paragraphs' || $field_settings['handler_settings']['filter']['type'] !== 'target_bundles') {
    $field->setSettings([
      'handler' => 'classy_paragraphs',
      'handler_settings' => [
        'filter' => [
          'type' => 'target_bundles',
          'target_bundles' => $target_bundles,
          'negate' => $negate ? 1 : 0,
        ],
        'sort' => ['field' => '_none'],
      ],
    ]);
  }
  else {
    if ($merge) {
      $existing_target_bundles = array_filter($field_settings['handler_settings']['filter']['target_bundles']);
      $target_bundles = array_merge($existing_target_bundles, $target_bundles);
    }
    $field_settings['handler_settings']['filter']['target_bundles'] = $target_bundles;
    $field_settings['handler_settings']['filter']['negate'] = $negate ? 1 : 0;
    $field->setSettings($field_settings);
  }
  $field->save();
}
