<?php

namespace Drupal\agoraparagraphs_youtube\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'youtube_default' widget.
 *
 * @FieldWidget(
 *   id = "youtube_default",
 *   label = @Translation("YouTube video/playlist"),
 *   field_types = {
 *     "youtube"
 *   },
 * )
 */
class YoutubeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['youtube_id'] = [
      '#title' => $this->t('YouTube ID'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->youtube_id,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => 255,
    ];

    $element['item_type'] = [
      '#title' => $this->t('Item type (video or playlist)'),
      '#type' => 'select',
      '#options' => [
        'l' => $this->t('Playlist'),
        'v' => $this->t('Video'),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#empty_value' => '',
      '#default_value' => $items[$delta]->item_type,
    ];

    $element['autoplay'] = [
      '#title' => $this->t('Auto play'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->autoplay),
    ];

    $element['mute'] = [
      '#title' => $this->t('Mute'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->mute),
    ];

    $element['fs'] = [
      '#title' => $this->t('Full screen'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->fs),
    ];

    $element['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->loop),
    ];

    $element['modestbranding'] = [
      '#title' => $this->t('Modest branding'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->mute),
    ];

    $element['rel'] = [
      '#title' => $this->t('Show related videos'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->rel),
    ];

    $element['showinfo'] = [
      '#title' => $this->t('Show info'),
      '#type' => 'checkbox',
      '#default_value' => !empty($items[$delta]->loop),
    ];

    $element['cc_load_policy'] = [
      '#title' => $this->t('CC load policy'),
      '#type' => 'number',
      '#default_value' => $items[$delta]->cc_load_policy ?? 3,
      '#min' => 0,
      '#max' => 3,
    ];

    $element['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'number',
      '#default_value' => $items[$delta]->width ?? 640,
      '#min' => 0,
    ];

    $element['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'number',
      '#default_value' => $items[$delta]->height ?? 385,
      '#min' => 0,
    ];
    return $element;
  }

}
