<?php

namespace Drupal\agoraparagraphs_youtube\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'youtube_default' formatter.
 *
 * @FieldFormatter(
 *   id = "youtube_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "youtube",
 *   },
 * )
 */
class YoutubeDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'youtube_embed',
        '#item' => $item,
      ];
    }
    $element['#attached'] = [
      'library' => [
        'agoraparagraphs_youtube/responsive_video',
      ],
    ];

    return $element;
  }

}
