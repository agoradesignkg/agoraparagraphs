<?php

namespace Drupal\agoraparagraphs_youtube\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'youtube' field type.
 *
 * @FieldType(
 *   id = "youtube",
 *   label = @Translation("YouTube video or playlist"),
 *   description = @Translation("This field allows to embed a YouTube video or playlist, along with some configuration options."),
 *   category = @Translation(" General"),
 *   default_widget = "youtube_default",
 *   default_formatter = "youtube_default"
 * )
 */
class YoutubeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'youtube_id' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
        ],
        'item_type' => [
          'type' => 'varchar',
          'length' => 1,
        ],
        'autoplay' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'mute' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'fs' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'loop' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'modestbranding' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'rel' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'showinfo' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'cc_load_policy' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
        ],
        'width' => [
          'type' => 'int',
          'size' => 'small',
          'unsigned' => TRUE,
        ],
        'height' => [
          'type' => 'int',
          'size' => 'small',
          'unsigned' => TRUE,
        ],
      ],
      'indexes' => [
        'youtube_id' => ['youtube_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['youtube_id'] = DataDefinition::create('string')
      ->setLabel(t('YouTube ID'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);

    $properties['item_type'] = DataDefinition::create('string')
      ->setLabel(t('Item type (video or playlist)'))
      ->addConstraint('Length', ['max' => 1])
      ->setRequired(TRUE);

    $properties['autoplay'] = DataDefinition::create('boolean')
      ->setLabel(t('Auto play'))
      ->setRequired(TRUE);

    $properties['mute'] = DataDefinition::create('boolean')
      ->setLabel(t('Mute'))
      ->setRequired(TRUE);

    $properties['fs'] = DataDefinition::create('boolean')
      ->setLabel(t('Allow full screen'))
      ->setRequired(TRUE);

    $properties['loop'] = DataDefinition::create('boolean')
      ->setLabel(t('Loop'))
      ->setRequired(TRUE);

    $properties['modestbranding'] = DataDefinition::create('boolean')
      ->setLabel(t('Modest branding'))
      ->setRequired(TRUE);

    $properties['rel'] = DataDefinition::create('boolean')
      ->setLabel(t('Show related videos'))
      ->setRequired(TRUE);

    $properties['showinfo'] = DataDefinition::create('boolean')
      ->setLabel(t('Show info'))
      ->setRequired(TRUE);

    $properties['cc_load_policy'] = DataDefinition::create('integer')
      ->setLabel(t('CC load policy'))
      ->setRequired(TRUE);

    $properties['width'] = DataDefinition::create('integer')
      ->setLabel(t('Width'));

    $properties['height'] = DataDefinition::create('integer')
      ->setLabel(t('Height'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'youtube_id';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->youtube_id) || empty($this->item_type);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [
      'youtube_id' => 'C0f96HQbCY4',
      'item_type' => 'v',
      'autoplay' => TRUE,
      'mute' => FALSE,
      'fs' => TRUE,
      'loop' => TRUE,
      'modestbranding' => TRUE,
      'rel' => FALSE,
      'showinfo' => FALSE,
      'cc_load_policy' => 3,
      'width' => 640,
      'height' => 385,
    ];
    return $values;
  }

}
